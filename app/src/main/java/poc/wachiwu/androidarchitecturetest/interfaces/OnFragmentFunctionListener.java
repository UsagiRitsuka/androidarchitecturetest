package poc.wachiwu.androidarchitecturetest.interfaces;

import poc.wachiwu.androidarchitecturetest.fragment.BaseFragment;

public interface OnFragmentFunctionListener {
    void startNewFragment(BaseFragment fragment);
}
