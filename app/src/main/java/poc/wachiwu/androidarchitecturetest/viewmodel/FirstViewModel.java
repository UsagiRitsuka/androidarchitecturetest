package poc.wachiwu.androidarchitecturetest.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import poc.wachiwu.androidarchitecturetest.bean.User;

public class FirstViewModel extends ViewModel {
    private String id;
    private MutableLiveData<User> user;

    public void init(String id){
        this.id = id;
    }

    public LiveData<User> getUser(){
        if(user == null){
            user = new MutableLiveData<>();
            user.setValue(new User("Init Name"));
        }

        return user;
    }
}
