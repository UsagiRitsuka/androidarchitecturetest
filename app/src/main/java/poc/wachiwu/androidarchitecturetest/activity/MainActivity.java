package poc.wachiwu.androidarchitecturetest.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;

import poc.wachiwu.androidarchitecturetest.R;
import poc.wachiwu.androidarchitecturetest.fragment.BaseFragment;
import poc.wachiwu.androidarchitecturetest.fragment.FirstFragment;
import poc.wachiwu.androidarchitecturetest.interfaces.OnFragmentFunctionListener;

public class MainActivity extends BaseActivity implements OnFragmentFunctionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startApp();
    }

    private void startApp(){
        FirstFragment fragment = new FirstFragment();
        fragment.setOnFragmentFunctionListener(this);

        getSupportFragmentManager()
            .beginTransaction()
            .addToBackStack(FirstFragment.class.getName())
            .add(R.id.frame, fragment)
            .commitAllowingStateLoss();
    }

    @Override
    protected void findView() {

    }

    @Override
    protected int getResId() {
        return R.layout.activity_main;
    }

    @Override
    public void startNewFragment(@NonNull BaseFragment fragment) {
        fragment.setOnFragmentFunctionListener(this);
        getSupportFragmentManager()
            .beginTransaction()
            .addToBackStack(fragment.getClass().getName())
            .replace(R.id.frame, fragment)
            .commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        getSupportFragmentManager().popBackStackImmediate();
    }
}
