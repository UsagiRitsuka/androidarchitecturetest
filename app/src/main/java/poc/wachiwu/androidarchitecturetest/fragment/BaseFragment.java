package poc.wachiwu.androidarchitecturetest.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import poc.wachiwu.androidarchitecturetest.interfaces.OnFragmentFunctionListener;

abstract public class BaseFragment extends Fragment {
    protected ViewGroup container;
    protected OnFragmentFunctionListener onFragmentFunctionListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(this.container == null){
            this.container = (ViewGroup) inflater.inflate(getResId(), null, false);

            findView(this.container);
            setupViewComponent();
        } else {
            ViewGroup viewGroup = (ViewGroup)this.container.getParent();
            if(viewGroup != null){
                viewGroup.removeView(this.container);
            }
        }


        return this.container;
    }

    public void setOnFragmentFunctionListener(OnFragmentFunctionListener onFragmentFunctionListener) {
        this.onFragmentFunctionListener = onFragmentFunctionListener;
    }

    abstract protected void findView(ViewGroup parent);
    abstract protected void setupViewComponent();
    abstract  protected int getResId();
}
