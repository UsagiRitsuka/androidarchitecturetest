package poc.wachiwu.androidarchitecturetest.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;

import poc.wachiwu.androidarchitecturetest.R;
import poc.wachiwu.androidarchitecturetest.bean.User;
import poc.wachiwu.androidarchitecturetest.viewmodel.FirstViewModel;

public class FirstFragment extends BaseFragment{
    private TextView textView;
    private Button button;
    private FirstViewModel firstViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        firstViewModel = ViewModelProviders.of(this).get(FirstViewModel.class);
        firstViewModel.init("12345");
        firstViewModel.getUser().observeForever(new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if(textView != null && user != null){
                    textView.setText(user.getName());
                }
            }
        });


    }

    @Override
    protected void findView(ViewGroup parent) {
        textView = parent.findViewById(R.id.text_view);
        button = parent.findViewById(R.id.btn);
    }

    @Override
    protected int getResId() {
        return R.layout.fragment_first;
    }

    @Override
    protected void setupViewComponent() {
        RxView.clicks(button)
            .subscribe(aVoid -> {
                if(onFragmentFunctionListener != null){
                    SecondFragment fragment = new SecondFragment();
                    fragment.setFirstViewModel(firstViewModel);
                    onFragmentFunctionListener.startNewFragment(fragment);
                }
            });
    }
}
