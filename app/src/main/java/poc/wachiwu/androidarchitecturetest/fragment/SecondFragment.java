package poc.wachiwu.androidarchitecturetest.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import poc.wachiwu.androidarchitecturetest.R;
import poc.wachiwu.androidarchitecturetest.viewmodel.FirstViewModel;

public class SecondFragment extends BaseFragment{
    private EditText editText;
    private FirstViewModel firstViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void findView(ViewGroup parent) {
        editText = parent.findViewById(R.id.edit);
    }

    @Override
    protected void setupViewComponent() {
        RxTextView.textChanges(editText)
            .subscribe(charSequence -> {
                if(firstViewModel != null){
                    firstViewModel.getUser().getValue().setName(charSequence.toString());
                }
            });
    }

    @Override
    protected int getResId() {
        return R.layout.fragment_second;
    }

    public void setFirstViewModel(FirstViewModel firstViewModel) {
        this.firstViewModel = firstViewModel;
    }
}
